# Jak přidat placení Bitcoinem přes BTCPay server do Shoptetu

1. V administraci Shoptetu v Nastavení -> Doprava a platba přidej platební metodu typu _Hotově_ s názvem např. _Bitcoinem_ (název musí obsahovat slovo "Bitcoin") a popisem např. _On-chain nebo Lightning network, pomocí BTCPay Serveru_. Nezapomeň vyplnit i Možnosti dopravy a Ceník.
2. V administraci Shoptetu ve Vzhled a obsah -> Editor -> HTML kód -> Dokončená objednávka vlož tento kód a uprav data atributy:
```
<script src="https://janpoboril.gitlab.io/shoptet-btcpay/checkout.js" data-store-id="M2Ppru1LLbVAv3tVtCMXkPxrmZWvtdVfYhm98rMMuTbh" data-server-url="https://btcpay.example.com" data-notify-email="tvuj@email.cz"></script>
```

Zákazník pak bude moci pomocí popupu BTCPay serveru zaplatit. Po zaplacení přijde notifikace na e-mail z `data-notify-email`, ale platbu je potřeba do Shoptetu vložit ručně.

> Pokud si nevíš rady, tak vždy rád pomůžu: https://www.berubitcoin.cz
