var storeId = document.currentScript.getAttribute('data-store-id')
var serverUrl = document.currentScript.getAttribute('data-server-url')
var notifyEmail = document.currentScript.getAttribute('data-notify-email')

function onBTCPayFormSubmit(event) {
    event.preventDefault()
    event.target.style.opacity = 0.5
    var xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200 && this.responseText) {
            window.btcpay.showInvoice(JSON.parse(this.responseText).invoiceId)
            window.btcpay.onModalReceiveMessage(console.log)
        }
    }
    xhttp.open('POST', event.target.getAttribute('action'), true)
    xhttp.send(new FormData(event.target))
}

document.addEventListener("DOMContentLoaded", function() {
    const recapPaymentMethod = document.querySelector('[data-testid="recapPaymentMethod"]')
    if (recapPaymentMethod && recapPaymentMethod.textContent.includes('Bitcoin')) {
        const price = /([\d\s]+)/.exec(document.querySelector('[data-testid="orderTotalPrice"]').textContent)[1].replace(/\W/g, '')
        const orderId = /(\d+)/.exec(document.querySelector('[data-testid="orderNumber"]').textContent)[1]
        if (!window.btcpay) {
            var script = document.createElement('script')
            script.src = serverUrl + "/modal/btcpay.js"
            document.getElementsByTagName('head')[0].append(script)
        }
        const button = document.createElement('div')
        button.innerHTML = `
            <p>
                <form class="order-complete-links" method="POST" onsubmit="onBTCPayFormSubmit(event);return false" action="${serverUrl}/api/v1/invoices">
                    <input type="hidden" name="storeId" value="${storeId}" />
                    <input type="hidden" name="jsonResponse" value="true" />
                    <input type="hidden" name="notifyEmail" value="${notifyEmail}" />
                    <input type="hidden" name="price" value="${price}" />
                    <input type="hidden" name="orderId" value="${orderId}" />
                    <input type="hidden" name="currency" value="CZK" />
                    <br>
                    <input type="submit" class="btn btn-lg btn-conversion" name="submit" value="Zaplatit Bitcoinem">
                </form>
            </p>
        `
        document.querySelector('[data-testid="recapTable"]').append(button)
    }
})
